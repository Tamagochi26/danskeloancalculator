﻿namespace Contracts.API.V1
{
    public class LoanCalculationResponse
    {
        public decimal EffectiveApr { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal TotalInterestAmount { get; set; }
        public decimal TotalAdministrativeFeeAmount { get; set; }
    }
}
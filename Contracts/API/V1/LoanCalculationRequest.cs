﻿using System.ComponentModel.DataAnnotations;

namespace Contracts.API.V1
{
    public class LoanCalculationRequest
    {
        [Range(0.01, double.MaxValue)]
        public decimal Amount { get; set; }
        [Range(1, 1200)]
        public int DurationInMonths { get; set; }
    }
}
﻿using AutoFixture.Xunit2;
using Domain.ValueObjects;
using Xunit;

namespace Domain.UnitTests
{
    public class LoanOverview_Should
    {
        [Theory]
        [InlineAutoData(0.1115522, 0.11155)]
        [InlineAutoData(0.999999, 1)]
        [InlineAutoData(0.99999, 0.99999)]
        [InlineAutoData(0.11, 0.11)]
        public void Round_EffectiveApr(
            decimal effectiveApr,
            decimal expectedResult,
            decimal paymentPerPeriod,
            decimal totalInterestAmount,
            decimal totalAdministrativeFeeAmount)
        {
            var sut = new LoanOverview(
                effectiveApr,
                paymentPerPeriod,
                totalInterestAmount,
                totalAdministrativeFeeAmount);

            Assert.Equal(expectedResult, sut.EffectiveApr);
        }

        [Theory]
        [InlineAutoData(123.55, 123.55)]
        [InlineAutoData(123.111111, 123.11)]
        [InlineAutoData(0.99999, 1)]
        public void Round_PaymentPerPeriod(
            decimal paymentPerPeriod,
            decimal expectedResult,
            decimal effectiveApr,
            decimal totalInterestAmount,
            decimal totalAdministrativeFeeAmount)
        {
            var sut = new LoanOverview(
                effectiveApr,
                paymentPerPeriod,
                totalInterestAmount,
                totalAdministrativeFeeAmount);

            Assert.Equal(expectedResult, sut.PaymentPerPeriod);
        }

        [Theory]
        [InlineAutoData(123.55, 123.55)]
        [InlineAutoData(123.111111, 123.11)]
        [InlineAutoData(0.99999, 1)]
        public void Round_TotalInterestAmount(
            decimal totalInterestAmount,
            decimal expectedResult,
            decimal effectiveApr,
            decimal paymentPerPeriod,
            decimal totalAdministrativeFeeAmount)
        {
            var sut = new LoanOverview(
                effectiveApr,
                paymentPerPeriod,
                totalInterestAmount,
                totalAdministrativeFeeAmount);

            Assert.Equal(expectedResult, sut.TotalInterestAmount);
        }

        [Theory]
        [InlineAutoData(123.55, 123.55)]
        [InlineAutoData(123.111111, 123.11)]
        [InlineAutoData(0.99999, 1)]
        public void Round_TotalAdministrativeFeeAmount(
            decimal totalAdministrativeFeeAmount,
            decimal expectedResult,
            decimal effectiveApr,
            decimal paymentPerPeriod,
            decimal totalInterestAmount)
        {
            var sut = new LoanOverview(
                effectiveApr,
                paymentPerPeriod,
                totalInterestAmount,
                totalAdministrativeFeeAmount);

            Assert.Equal(expectedResult, sut.TotalAdministrativeFeeAmount);
        }
    }
}
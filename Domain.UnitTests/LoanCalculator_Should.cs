using AutoFixture.Xunit2;
using Domain.ValueObjects;
using System;
using Xunit;

namespace Domain.UnitTests
{
    public class LoanCalculator_Should
    {
        [Fact]
        public void Calculate_LoanOverview_As_In_Example()
        {
            var loanTerms = new LoanTerms(0.05m, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(500000, 120);

            Assert.Equal(5303.28m, result.PaymentPerPeriod);
            Assert.Equal(136393.09m, result.TotalInterestAmount);
            Assert.Equal(5000m, result.TotalAdministrativeFeeAmount);
        }

        [Fact]
        public void Calculate_EffectiveApr()
        {
            var loanTerms = new LoanTerms(0.1m, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(500000, 120);

            Assert.Equal(0.10471m, result.EffectiveApr);
        }

        [Fact]
        public void Calculate_MonthlyPayment()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(599.55m, result.PaymentPerPeriod);
        }

        [Fact]
        public void Calculate_MonthlyPayment_When_Interest_Is_0()
        {
            var loanTerms = new LoanTerms(0, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(12000, 12);

            Assert.Equal(1000, result.PaymentPerPeriod);
        }

        [Fact]
        public void Calculate_TotalInterestAmount()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(115838.19m, result.TotalInterestAmount);
        }

        [Fact]
        public void Calculate_TotalInterestAmount_When_Interest_Is_0()
        {
            var loanTerms = new LoanTerms(0, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(0, result.TotalInterestAmount);
        }

        [Fact]
        public void Calculate_TotalAdministrativeFeeAmount_When_Lower_Than_Max()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(1000, result.TotalAdministrativeFeeAmount);
        }

        [Fact]
        public void Calculate_TotalAdministrativeFeeAmount_When_Fee_Is_0()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0, 10000m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(0, result.TotalAdministrativeFeeAmount);
        }

        [Fact]
        public void Calculate_TotalAdministrativeFeeAmount_When_Maxed()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 500m);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(500, result.TotalAdministrativeFeeAmount);
        }

        [Fact]
        public void Calculate_TotalAdministrativeFeeAmount_When_Max_Is_0()
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 0);
            var sut = new LoanCalculator(loanTerms);

            var result = sut.CalculateLoanOverview(100000, 360);

            Assert.Equal(0, result.TotalAdministrativeFeeAmount);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-70)]
        public void Throw_On_InvalidAmount(decimal amount)
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 500m);
            var sut = new LoanCalculator(loanTerms);

            Assert.Throws<ArgumentOutOfRangeException>("amount", () => sut.CalculateLoanOverview(amount, 360));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-10)]
        public void Throw_On_Invalid_NumberOfPeriods(int numberOfPeriods)
        {
            var loanTerms = new LoanTerms(0.06m, PaymentFrequencyPerYear.Monthly, 0.01m, 500m);
            var sut = new LoanCalculator(loanTerms);

            Assert.Throws<ArgumentOutOfRangeException>("numberOfPeriods", () => sut.CalculateLoanOverview(10000, numberOfPeriods));
        }
    }
}
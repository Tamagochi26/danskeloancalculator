﻿using Domain.ValueObjects;

namespace WebAPI
{
    public class LoanTermsConfiguration
    {
        public decimal AnnualInterestRate { get; set; }
        public PaymentFrequencyPerYear PaymentFrequencyPerYear { get; set; }
        public decimal AdministrativeFee { get; set; }
        public decimal MaxAdministrativeFee { get; set; }
    }
}
﻿using Contracts.API.V1;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DanskeLoadCalculator.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class LoanCalculatorController : ControllerBase
    {
        private readonly ILoanCalculator _loanCalculator;

        public LoanCalculatorController(ILoanCalculator loanCalculator)
        {
            _loanCalculator = loanCalculator;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<LoanCalculationResponse> Calculate([FromBody]LoanCalculationRequest request)
        {
            var result = _loanCalculator.CalculateLoanOverview(request.Amount, request.DurationInMonths);
            return Ok(result);
        }
    }
}
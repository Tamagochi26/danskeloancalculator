using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAPI;
using Domain;
using Domain.ValueObjects;

namespace DanskeLoadCalculator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen();

            var loanTermsConfiguration = new LoanTermsConfiguration();
            Configuration.GetSection("LoanTerms").Bind(loanTermsConfiguration);
            services.AddSingleton(loanTermsConfiguration);

            services.AddScoped(x => {
                var configuration = x.GetRequiredService<LoanTermsConfiguration>();
                return new LoanTerms(
                    configuration.AnnualInterestRate,
                    configuration.PaymentFrequencyPerYear,
                    configuration.AdministrativeFee,
                    configuration.MaxAdministrativeFee);
                });
            services.AddScoped<ILoanCalculator, LoanCalculator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

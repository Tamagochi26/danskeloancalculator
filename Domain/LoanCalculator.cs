﻿using Domain.ValueObjects;
using System;

namespace Domain
{
    public class LoanCalculator : ILoanCalculator
    {
        private LoanTerms _loanTerms;

        public LoanCalculator(LoanTerms loanTerms)
        {
            _loanTerms = loanTerms;
        }

        public LoanOverview CalculateLoanOverview(
            decimal amount,
            int numberOfPeriods)
        {
            if (amount <= 0)
                throw new ArgumentOutOfRangeException(nameof(amount));
            if (numberOfPeriods <= 0)
                throw new ArgumentOutOfRangeException(nameof(numberOfPeriods));

            var interestRatePerPeriod = _loanTerms.AnnualInterestRate / (int)_loanTerms.PaymentFrequencyPerYear;
            var effectiveApr = CalculateEffectiveApr(interestRatePerPeriod);
            var paymentPerPeriod = CalculatePaymentPerPeriod(amount, interestRatePerPeriod, numberOfPeriods);
            var totalInterestAmount = CalculateTotalInterestAmount(amount, paymentPerPeriod, numberOfPeriods);
            var totalAdministrativeAmount = CalculateAdministrativeFee(amount);

            return new LoanOverview(
                effectiveApr,
                paymentPerPeriod,
                totalInterestAmount,
                totalAdministrativeAmount);
        }

        private decimal CalculateEffectiveApr(decimal interestRatePerPeriod)
        {
            return (decimal) Math.Pow(1 + (double)interestRatePerPeriod, (int)_loanTerms.PaymentFrequencyPerYear) - 1;
        }

        private decimal CalculatePaymentPerPeriod(decimal amount, decimal interestRatePerPeriod, int numberOfPeriods)
        {
            if (interestRatePerPeriod == 0)
                return amount / numberOfPeriods;

            var interestRateProduct = (decimal) Math.Pow(1 + (double)interestRatePerPeriod, numberOfPeriods);
            return amount / ((interestRateProduct - 1) / (interestRatePerPeriod * interestRateProduct));
        }

        private decimal CalculateTotalInterestAmount(decimal amount, decimal paymentPerPeriod, int numberOfPeriods)
        {
            return numberOfPeriods * paymentPerPeriod - amount;
        }

        private decimal CalculateAdministrativeFee(decimal amount)
        {
            return Math.Min(amount * _loanTerms.AdministrativeFee, _loanTerms.MaxAdministrativeFee);
        }
    }
}

﻿using System;

namespace Domain.ValueObjects
{
    public class LoanTerms
    {
        public LoanTerms(
            decimal annualInterestRate,
            PaymentFrequencyPerYear paymentFrequencyPerYear,
            decimal administrativeFee,
            decimal maxAdministrativeFee)
        {
            if (annualInterestRate < 0)
                throw new ArgumentOutOfRangeException(nameof(annualInterestRate));
            if (administrativeFee < 0)
                throw new ArgumentOutOfRangeException(nameof(administrativeFee));
            if (maxAdministrativeFee < 0)
                throw new ArgumentOutOfRangeException(nameof(maxAdministrativeFee));

            AnnualInterestRate = annualInterestRate;
            PaymentFrequencyPerYear = paymentFrequencyPerYear;
            AdministrativeFee = administrativeFee;
            MaxAdministrativeFee = maxAdministrativeFee;
        }

        public decimal AnnualInterestRate { get; }
        public PaymentFrequencyPerYear PaymentFrequencyPerYear { get; }
        public decimal AdministrativeFee { get; }
        public decimal MaxAdministrativeFee { get; }
    }
}
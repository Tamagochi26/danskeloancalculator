﻿using System;

namespace Domain.ValueObjects
{
    public class LoanOverview
    {
        public LoanOverview(
            decimal effectiveApr,
            decimal paymentPerPeriod,
            decimal totalInterestAmount,
            decimal totalAdministrativeFeeAmount)
        {
            EffectiveApr = Math.Round(effectiveApr, 5);
            PaymentPerPeriod = Math.Round(paymentPerPeriod, 2);
            TotalInterestAmount = Math.Round(totalInterestAmount, 2);
            TotalAdministrativeFeeAmount = Math.Round(totalAdministrativeFeeAmount, 2);
        }

        public decimal EffectiveApr { get; }
        public decimal PaymentPerPeriod { get; }
        public decimal TotalInterestAmount { get; }
        public decimal TotalAdministrativeFeeAmount { get; }
    }
}
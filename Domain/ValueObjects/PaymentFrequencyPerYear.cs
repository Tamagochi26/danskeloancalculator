﻿namespace Domain.ValueObjects
{
    public enum PaymentFrequencyPerYear
    {
        Monthly = 12
    }
}
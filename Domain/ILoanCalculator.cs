﻿using Domain.ValueObjects;

namespace Domain
{
    public interface ILoanCalculator
    {
        LoanOverview CalculateLoanOverview(decimal amount, int numberOfPeriods);
    }
}